<?php 
$sucursales = ControladorSucursal::ctrMostrarSucursales(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
 
foreach ($sucursales as $key => $value) { 
 
$contador = $key+1; 
} 
?>

<div id="back"></div>

<div class="login-box">
  
  <div class="login-logo">

    <img src="vistas/img/plantilla/logo-blanco-bloque.png" class="img-responsive">

  </div>

  <div class="login-box-body">

    <p class="login-box-msg"><b>Iniciar Sesión</p>

    <form method="post">

      <div class="form-group has-feedback">

        <input type="text" class="form-control" placeholder="Usuario" name="ingUsuario" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>

      </div>

      <div class="form-group has-feedback">

        <input type="password" class="form-control" placeholder="Contraseña" name="ingPassword" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      
      </div>

      <div class="form-group has-feedback">

            <!-- ENTRADA PARA SELECCIONAR CATEGORÍA --> 
 
            <?php if ($contador > 1): ?> 
               
                <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
               
                <select class="form-control" id="ingSucursal" name="ingSucursal" required> 
                   
                  <option value="">Selecionar Sucursal</option> 
 
                  <?php 
 
                   foreach ($sucursales as $key => $value) { 
                     
                    echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>'; 
                  } 
 
                  ?> 
   
                </select> 
 
            <?php endif ?> 
      
      </div>

      <div class="row">
       
        <div class="col-xs-4">

          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        
        </div>

      </div>

      <?php

        $login = new ControladorUsuarios();
        $login -> ctrIngresoUsuario();
        
      ?>

    </form>

  </div>

</div>
