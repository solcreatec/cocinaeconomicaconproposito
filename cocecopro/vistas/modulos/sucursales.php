<?php

// if($_SESSION["tipousuario_id"] == 3){

//   echo '<script>

//     window.location = "inicio";

//   </script>';

//   return;

// }

// if ($_SESSION["tipousuario_id"] == 1) {
//   $sucursales = ControladorSucursal::ctrMostrarSucursales(null,null,null,null,null,null,null,null,null,null);
// }else {
//   $Sucursales = ControladorSucursal::ctrMostrarSucursales("usuario_id",$_SESSION['id'],null,null,null,null,null,null,null,null);
// }

$sucursales = ControladorSucursal::ctrMostrarSucursales(null,null);
$estados = ControladorEstado::ctrMostrarEstados(NULL, NULL);
$municipios = ControladorMunicipio::ctrMostrarMunicipios(NULL, NULL);

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Sucursales
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Sucursales</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">
<?php if($_SESSION["tipousuario_id"] == 1){   ?>
      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          
          <i class="fa fa-plus-circle"></i>
          &nbsp;&nbsp;Agregar

        </button>

      </div>
<?php } ?>
      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
            <th style="width:10px">#</th>
            <th width="5%">ID</th>
            <th>Nombre</th>
            <th>Direccion</th>
            <th width="9%">Telefono</th>
<!--             <th>Horario</th>
            <th>Encargado</th> -->
            <th width="7%">Estatus</th>

            <?php if($_SESSION["tipousuario_id"] == 1){  ?>
            <!-- <th>Acciones</th> -->
            <?php } ?>

         </tr> 

        </thead>

        <tbody>

        <?php

         foreach ($sucursales as $key => $value) {
            

            echo '<tr>

                    <td>'.($key+1).'</td>

                    <td>'.$value["id"].'</td>

                    <td>'.$value["nombre"].'</td>

                    <td>'.$value["direccion"].' Col.'.$value["colonia"].' CP.'.$value["cp"].'</td>

                    <td>'.$value["telefono"].'</td>';

                    // if ($value["horaapertura"] == NULL) {
                    //   '<td><B>No Definido</b></td>';
                    // }else{

                    // echo '<td>'.$value["horaapertura"].' A '.$value["horacierre"].'</td>';
                    // } 

                    // echo '<td>'.$value["usuario_id"].'</td>             
                      if($value['estatusventa'] == 1) { 

                        echo "<td><p class='label label-success'>
                                <i class='glyphicon glyphicon-ok'></i> 
                                &nbsp;&nbsp;Finalizado
                              </p>"; 
                            }
                      else { 

                        echo "<td><p class='label label-success'>
                               <i class='glyphicon glyphicon-ok'></i> 
                               &nbsp;&nbsp;En Operacion
                              </p></td>"; 
                            }

                    echo '


                  </tr>';
                    //           <td>

                    //   <div class="btn-group">';
                    //   if($_SESSION["tipousuario_id"] == 1){             
                    //     echo '<button class="btn btn-warning btnEditarSucursal" data-toggle="modal" data-target="#modalEditarSucursal" idSucursal="'.$value["id"].'"><i class="fa fa-pencil"></i></button>';

                    //       echo '<button class="btn btn-danger btnEliminarSucursal" idSucursal="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                    //   }

                    //   echo '</div>  

                    // </td>
            }

        ?>
   
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR SUCURSAL
======================================-->

<div id="modalAgregarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
          <i class="fa fa-plus" aria-hidden="true"></i>

          &nbsp;&nbsp;Nueva Sucursal</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">


            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></i></span> 

                <input type="text" class="form-control input-lg" required name="nuevoSucursal" id="nuevoSucursal" placeholder="Ingresa un nombre o alias" required/>  

              </div>

            </div>

            <!-- ENTRADA PARA LA DIRECCIÓN -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 

                <input type="text" class="form-control input-lg" id="direccion" name="direccion" placeholder="Ingresar dirección" required>

              </div>

            </div>


             <!-- ENTRADA PARA LA COLONIA -->

             <div class="form-group row">

                <div class="col-xs-6">
                
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span> 

                      <input type="text" class="form-control input-lg" name="colonia" id="colonia" placeholder="Ingresa Colonia" required>

                  </div>

                </div>

                <!-- ENTRADA PARA CODIGO POSTAL -->

                <div class="col-xs-6">
                
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="glyphicon glyphicon-screenshot"></i></span> 

                      <input type="text" class="form-control input-lg" maxlength="5" required id="cp" name="cp" placeholder="Código Postal" required/>  

                  </div>
                
                  <br>


                </div>

            </div>


             <!-- ENTRADA PARA EL ESTADO DE LA REPUBLICA -->

             <div class="form-group row">

                <div class="col-xs-6">
                
                  <div class="input-group">
                  
                  <select class="form-control input-lg" required name="estado" id="estado" required>
                      <option value=""></option>
                      <?php foreach ($estados as $key => $value) {
                      ?>
                      <option value="<?php echo $value["id"]; ?>"><?php echo $value["nombre"]; ?></option>
                      <?php }?>

                  </select>
                  </div>

                </div>

                <!-- ENTRADA PARA EL MUNICIPIO -->

                <div class="col-xs-6">
                
                  <div class="input-group">
                  
                    <select class="form-control input-lg" required name="municipio" id="municipio" required>
                        <option value=""></option>
                        <?php foreach ($municipios as $key => $value) {
                        ?>
                        <option value="<?php echo $value["id"]; ?>"><?php echo $value["nombre"]; ?></option>
                        <?php }?>

                    </select>

                  </div>
                
                  <br>


                </div>

            </div>


            <!-- ENTRADA PARA EL EMAIL -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span> 

                <input type="email" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar email" required>

              </div>


            </div>

            <!-- ENTRADA PARA EL TELÉFONO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar teléfono" data-inputmask="'mask':'(999) 999-9999'" data-mask required>

              </div>

            </div>


 
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-success">

                   <i class="glyphicon glyphicon-saved"></i>
      Guardar</button>
          </div>
        </button>

        </div>

      </form>

      <?php

        // $crearCliente = new ControladorClientes();
        // $crearCliente -> ctrCrearCliente();

        $crearSucursal = new ControladorSucursal();
        $crearSucursal -> ctrCrearSucursal();

      ?>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR SUCURSAL
======================================-->

<div id="modalEditarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Sucursal</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control" name="editarCliente" id="editarCliente" required>
                <input type="hidden" id="idCliente" name="idCliente">
              </div>

            </div>

            <!-- ENTRADA PARA EL DOCUMENTO ID -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                <input type="number" min="0" class="form-control" name="editarDocumentoId" id="editarDocumentoId" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL EMAIL -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span> 

                <input type="email" class="form-control" name="editarEmail" id="editarEmail" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL TELÉFONO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control" name="editarTelefono" id="editarTelefono" data-inputmask="'mask':'(999) 999-9999'" data-mask required>

              </div>

            </div>

            <!-- ENTRADA PARA LA DIRECCIÓN -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 

                <input type="text" class="form-control" name="editarDireccion" id="editarDireccion"  required>

              </div>

            </div>

             <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 

                <input type="text" class="form-control" name="editarFechaNacimiento" id="editarFechaNacimiento"  data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>

              </div>

            </div>
  
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

      </form>

      <?php

        $editarCliente = new ControladorClientes();
        $editarCliente -> ctrEditarCliente();

      ?>

    

    </div>

  </div>

</div>

<?php

  $eliminarCliente = new ControladorClientes();
  $eliminarCliente -> ctrEliminarCliente();

?>


