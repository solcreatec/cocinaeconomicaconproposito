<?php
if($_SESSION["tipousuario_id"] == 3){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}


$totalCorte = 0;
$totalEfectivo = 0;
$totalTD = 0;
$totalTC = 0;
$totalVales = 0;

$cajero = ControladorUsuarios::ctrMostrarUsuarios("id", $_SESSION["id"]);


if ($_SESSION["tipousuario_id"] == 1) {
  $ventas = ControladorVentas::ctrVentasMostrar("estatusventa", "1", "corte_id", "0", "act", "1", NULL, NULL, NULL, NULL);
}else {
  $ventas = ControladorVentas::ctrVentasMostrar("estatusventa", "1", "corte_id", "0", "act", "1", "usuario_id", $_SESSION["id"], "sucursal_id", $_SESSION["sucursal_id"]);
}


// echo $item.$valor.$item2.$valor2.$item3.$valor3.$item4.$valor4; exit();



  foreach ($ventas as $key => $value) {

    $totalCorte += $value['total'];
    $pagos = ControladorPago::ctrPagosTicket("venta_id", $value['id']);

      if ($pagos['tipopago_id'] == 1) {
        $totalEfectivo += $pagos['total'];
      } else if ($pagos['tipopago_id'] == 2) {
        $totalTD += $pagos['total'];
      } else if ($pagos['tipopago_id'] == 3) {
        $totalTC += $pagos['total'];
      } else if ($pagos['tipopago_id'] == 4) {
        $totalVales += $pagos['total'];
      } 
  }

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Nuevo Corte de Caja</h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li><a href="cortecaja"> Cortes de Caja</a></li>
      
      <li class="active">Crear Corte</li>
    
    </ol>

  </section>

  <section class="content">
<p class="alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;
Una vez generado el corte ninguna venta podrá <b>CANCELARSE O MODIFICARSE</b></p>
    <div class="row">


<!--=====================================
LISTA DE PRODUCTOS DE LA VENTA
======================================-->

      <div class="col-lg-8 col-xs-12">
        
        <div class="box box-primary">

          <div class="box-header with-border"><h4><B>
            <i class="fa fa-list" aria-hidden="true"></i>
                &nbsp;Lista de Ventas</B></h4></div>

            <div class="box-body">
            
                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

          
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Código</th>
           <th>Vendedor</th>
           <th >Tipo Pago</th>
           <th>Fecha Venta</th> 
           <th>Total</th>

         </tr> 

        </thead>
          <?php

          foreach ($ventas as $key => $value) {
            
            $itemID = "id";
            $vendedor = ControladorUsuarios::ctrMostrarUsuarios("id", $value['usuario_id']); 
            $pagos = ControladorPago::ctrPagosTicket("venta_id", $value['id']);
            $tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$pagos['tipopago_id']);

            echo '<tr>

            <td>'.($key+1).'</td>
            <td>'.$value["codigo"].'</td>';

            echo '<td>'.$vendedor["nombre"].'</td>

            <td>'.$tipopago["nombre"].'</td>

            <td>'.strftime("%A, %d de %B de %G, %H:%M", strtotime($value["fechaalta"])).'</td>

            <td><B>$'.number_format($value["total"],2).'</td>
            </tr>
            ';

        }?>
        </table>

<!-- ==================================================================== -->


          </div>

        </div>

      </div>


      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-4 col-xs-12">
        
        <div class="box box-success">

          <form role="form" method="post" class="formularioCorte">
            
            <div class="box-body">
  
<!--=====================================
DETALLE VENTA PARA PANTALLA GRANDE
======================================-->


       <table class="table">
        <thead>
          <tr>
            <th>Codigo a Registrar para Corte</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td>
                <!--=====================================
                ENTRADA DEL CÓDIGO
                ======================================--> 

                  
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>

                    <?php
                    // echo $_SESSION['sucursal_id'];
                    $cortes = ControladorCorte::ctrCortesMostrar("sucursal_id",$_SESSION['sucursal_id'],null,null,null,null,null,null, null, null);

                    if(!$cortes){

                      echo '<input type="text" class="form-control" id="nuevoCorte" name="nuevoCorte" value="'.$_SESSION['sucursal_id'].'000000001" readonly>';

                    }else{

                      foreach ($cortes as $key => $value) {
                      }

                      $codigo = $value["codigo"] + 1;

                      echo '<input type="text" class="form-control" id="nuevoCorte" name="nuevoCorte" value="'.$codigo.'" readonly>';
                  
                    }

                    ?>
                    
                    
                  </div>
                
        </td>
        
      </tr>
    </tbody>
  </table>


                    
  <table class="table">
        <thead>
          <tr>
            <th>Cajero</th>
          </tr>
        </thead>

        <tbody>
          <tr>
             <td>
    <div class="input-group">
      
      <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
    </span>

     <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $cajero["nombre"]; ?>" readonly>
     <input type="hidden" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $_SESSION["id"]; ?>" readonly>
 
    </div>
            </td>
             
          </tr>
        </tbody>
      </table>


  <!--=====================================
  ENTRADA  DE TODOS LOS TOTALES
  ======================================-->
        <table class="table">

          <thead>

            <tr>
              <th></th>
 
            </tr>

          </thead>
          <tbody>
        <?php if ($totalEfectivo >0){ ?>
            <tr>
              <td style="width: 30%">

                <div class="input-group pull-right">
               
                  <h6><b>Efectivo
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="$ <?php echo number_format($totalEfectivo,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
          <?php } 

          if ($totalTD >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Tarjeta Debito
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalTD,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
        <?php } 

          if ($totalTC >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Tarjeta Credito
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalTC,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
          <?php } 

          if ($totalVales >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Vales Despensa
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalVales,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
          <?php } ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Total
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="TotalCorteVisual" name="TotalCorteVisual" value="<?php echo number_format($totalCorte,2); ?>" readonly required>
                  <input type="hidden" class="form-control" id="nuevoTotalCorte" name="nuevoTotalCorte" value="<?php echo $totalCorte; ?>" readonly required>
                  
                </div>

              </td>

            </tr>
          </tbody>

        </table>
  </div>
            <div class="box-footer">

            <button type="submit" class="btn btn-success pull-right">

              <i class="fa fa-download" aria-hidden="true"></i>

            &nbsp;&nbsp;Generar

            </button>

          </div>
        </form>
                <?php

          $guardarCorte = new ControladorCorte();
          $guardarCorte -> ctrCrearCorte();
          
        ?>
  </div>
</div>
  </section>
</div>

