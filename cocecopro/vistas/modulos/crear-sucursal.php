<?php
if($_SESSION["tipousuario_id"] == 3){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}


  $estados = ControladorEstado::ctrMostrarEstados(NULL, NULL);
  $municipios = ControladorMunicipio::ctrMostrarMunicipios(NULL, NULL);
  // $dias = ControladorDia::ctrMostrarDias(NULL, NULL);

if ($_SESSION["tipousuario_id"] == 1) {
  $ventas = ControladorVentas::ctrVentasMostrar("estatusventa", "1", "corte_id", "0", "act", "1", NULL, NULL, NULL, NULL);
}else {
  $ventas = ControladorVentas::ctrVentasMostrar("estatusventa", "1", "corte_id", "0", "act", "1", "usuario_id", $_SESSION["id"], NULL, NULL);
}


?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Nueva Sucursal</h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li><a href="sucursales"> Sucursales</a></li>
      
      <li class="active">Crear Sucursal</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="row">


      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-8 hidden-xs">
        
        <div class="box box-success">

          <form role="form" method="post" class="formularioVenta">
            
            <div class="box-body">
  
<!--=====================================
DETALLE VENTA PARA PANTALLA GRANDE
======================================-->

       <table class="table">
        <thead>
          <tr>
            <!-- <th>Numero</th> -->
            <th>Nombre o Alias</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <!-- <td width="20%"> -->
                <!--=====================================
                ENTRADA DEL CÓDIGO
                ======================================--> 
                  
<!--                   <div class="input-group">
                    
                    <span class="input-group-addon">
                      <i class="fa fa-hashtag" aria-hidden="true"></i>
                    </span>

                      <input type="text" class="form-control" id="nuevoSucursal" name="nuevoSucursal" value="SUC-0" readonly>
                    
                  </div>
                
            </td> -->

            <td>
            <!--=====================================
            ENTRADA NOMBRE DE LA SUCURSAL
            ======================================--> 
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon">
                      <i class="fa fa-building-o" aria-hidden="true"></i>
                    </span>

                      <input type="text" class="form-control" required name="nombresucursal" id="nombresucursal" placeholder="Nombre Sucursal" required/>                
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================--> 

       <table class="table">
        <thead>
          <tr>
            <th>Calle</th>
            <th>No. Externo</th>
            <th>No. Interior</th>
          </tr>
        </thead>

        <tbody>
      <tr>

            <td width="70%">
            <!--=====================================
            ENTRADA CALLE DE LA SUCURSAL
            ======================================--> 
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon">
                      <i class="fa fa-building-o" aria-hidden="true"></i>
                    </span>

                      <input type="text" class="form-control" name="calle" id="calle" placeholder="Calle de la Sucursal" required>                
                  </div>
                
            </td>

            <td>
            <!--=====================================
            ENTRADA NUMERO EXTERIOR
            ======================================--> 
                  
                  <div class="input-group">
                      <input type="number" class="form-control" required name="numext" id="numext" placeholder="#" required/>                
                  </div>
                
            </td>

            <td>
            <!--=====================================
            ENTRADA NUMERO INTERIOR
            ======================================--> 
                  
                  <div class="input-group">
                      <input type="text" class="form-control" name="numint" id="numint" placeholder="#" >                
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================--> 

       <table class="table">
        <thead>
          <tr>
            <th>Colonia</th>
            <th>Codigo Postal</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td width="70%">
                <!--=====================================
                ENTRADA DE LA COLONIA
                ======================================--> 

                  
                  <div class="input-group">
                    
                    <span class="input-group-addon">
                      <i class="fa fa-hashtag" aria-hidden="true"></i>
                    </span>                  
                        <input type="text" class="form-control" required name="colonia" id="colonia" placeholder="Colonia de la sucursal" required/>
                  </div>
                
            </td>

            <td width="30%">
            <!--=====================================
            ENTRADA CODIGO POSTAL
            ======================================--> 
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon">
                      <i class="fa fa-building-o" aria-hidden="true"></i>
                    </span>

                      <input type="text" class="form-control" maxlength="6" required id="cp" name="cp" placeholder="Código Postal" required/>                
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================-->

       <table class="table">
        <thead>
          <tr>
            <th>Estado</th>
            <th>Ciudad</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td >
                <!--=====================================
                ENTRADA DEL ESTADO DE LA REPUBLICA
                ======================================--> 

                  
                  <div class="input-group">
                    
            <select class="form-control " required name="estado" id="estado" required placeholder="Estado donde esta la sucursal">
        <option value=""></option>
      <?php foreach ($estados as $key => $value) {
        ?>
        <option value="<?php echo $value["id"]; ?>"><?php echo $value["estado"]; ?></option>
      <?php }?>

      </select>
                  </div>
                
            </td>

            <td>
            <!--=====================================
            ENTRADA DEL MUNICIPIO 
            ======================================--> 
                  
                  <div class="input-group">
                    
            <select name="municipio" class="form-control " required name="municipio" id="municipio" required placeholder="Municipio donde esta la sucursal">
        <option value=""></option>
      <?php foreach ($municipios as $key => $value) {
        ?>
        <option value="<?php echo $value["idmunicipio"]; ?>"><?php echo $value["municipio"]; ?></option>
      <?php }?>

      </select>
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================-->  
       <table class="table">
        <thead>
          <tr>
            <th>Telefono</th>
            <th>E-Mail</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td>
                <!--=====================================
                ENTRADA DEL TELEFONO
                ======================================--> 
                  
                  <div class="input-group">
                    
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-phone-alt"></span>
                  </span>                
                        <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Ingresar teléfono" data-inputmask="'mask':'(999) 999-9999'" data-mask required>
                  </div>
                
            </td>

            <td>
            <!--=====================================
            ENTRADA DEL EMAIL
            ======================================--> 
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-envelope"></span>
                    </span>
                    <input type="email" class="form-control" name="email" name="email" placeholder="Ingresar email" required>            
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================-->
  </div>
  </div>
</div>


      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-4 xs-12">
        
        <div class="box box-success">
            
            <div class="box-body">
  

       <table class="table">
        <thead>
          <tr>
            <th>Fecha de Apertura</th>
          </tr>
        </thead>

        <tbody>
      <tr>

            <td>
            <!--=====================================
            ENTRADA FECHA EN QUE INICIO LA SUCURSAL
            ======================================--> 
                  
                  <div class="input-group">
                    
        <input class="form-control" id="fechaapertura" name="fechaapertura" required placeholder="Dia/Mes/Año"/>
          <script>
            $('#fechaapertura').datepicker({
            autoclose: true
            });
          </script>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>            
                  </div>
                
            </td>
          </tr>
    </tbody>
  </table>

<!--================================================================--> 

<!--        <table class="table">
        <thead>
          <tr>
            <th>Días de Operación</th>
          </tr>
        </thead>

        <tbody>
      <tr>

            <td>
 -->            <!--=====================================
            ENTRADA NOMBRE DE LA SUCURSAL
            ======================================--> 
                  
<!--                   <div class="input-group">
                    
                <select class="form-control input-sm selectpicker" multiple name="Datos[]" data-error="Es un campo obligatorio" required="required" id="diasoperacion" readonly title="Selecciona los dias que abrira la sucursal"> -->
        <!-- <option value="lunes" selected="selected">lunes</option> -->
<!--             <?php foreach ($dias as $key => $value) {
        ?>
        <option value="<?php echo $value["id"]; ?>"><?php echo $value["nombre"]; ?></option>
      <?php }?>
    </select>               
                  </div>
                
            </td>

          </tr>
    </tbody>
  </table> -->

<!--================================================================--> 

       <table class="table">
        <thead>
          <tr>
            <th>Horario</th>
          </tr>
        </thead>

        <tbody>
      <tr>
            <td>
            <!--=====================================
            ENTRADA DEL HORARIO
            ======================================--> 
                  
    <div  id="timeOnlyExample" class="input-group">
    <input type="text" class="form-control time start" required name="horaapertura" id="horaapertura" placeholder="Inicio"/>
          <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
          </span>

    <input type="text" class="form-control time end" required id="horacierre" name="horacierre" placeholder="Cierre"/>
          <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
          </span>
    </div>
               <script>
$('#timeOnlyExample .time').timepicker({
'showDuration': true,
'timeFormat': 'G:ia'
});

var timeOnlyExampleEl = document.getElementById('timeOnlyExample');
var timeOnlyDatepair = new Datepair(timeOnlyExampleEl);
</script> 
            </td>
          </tr>
    </tbody>
  </table>



<!--================================================================-->
  </div>
            <div class="box-footer">

            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-download" aria-hidden="true"></i>

            &nbsp;&nbsp;Generar</button>

          </div>
        </form>
                <?php

          $guardarSucursal = new ControladorSucursal();
          $guardarSucursal -> ctrCrearSucursal();
          
        ?>
  </div>
</div>

  </section>
</div>

