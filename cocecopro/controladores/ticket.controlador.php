<?php

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class ControladorTicket{

	static public function ctrImprimir($venta, $usuario){
		// echo $venta;
		$venta = ControladorVentas::ctrMostrarVentas("id", $venta);
		$sucursal = ControladorSucursal::ctrMostrarSucursales("id",$venta['sucursal_id']);
		$pagos = ControladorPago::ctrPagosTicket("venta_id", $venta['id']);
		$tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$pagos['tipopago_id']);

		$nombreImpresora = "Cocecopro"; 
		$conexion = new WindowsPrintConnector($nombreImpresora);
		$impresora = new Printer($conexion);

							# Alineacion al centro de lo próximo que imprimamos
							$impresora->setJustification(Printer::JUSTIFY_CENTER);

							/*
								IMPRESION DEL LOGOTIPO (Opcional)
								No funcionará en todas las impresoras
							 
								Nota: Recomendable que la imagen no sea
								transparente (aunque sea png hay que quitar el canal alfa)
								y que tenga una resolución baja. En mi caso
								la imagen que uso es de 250 x 250
							*/

							#Se trata de cargar e imprimir el logo.
							try{
								$logo = EscposImage::load("vistas/img/plantilla/logo.png", false);
							    $impresora->bitImage($logo);
							}catch(Exception $e){/*No hacemos nada si hay error*/}
							 
							#Encabezado							 
							$impresora->text("COCINA ECONOMICA" . "\n");
							$impresora->text("Con Próposito" . "\n");
							$impresora->text($sucursal['direccion']."\n");
							$impresora->text("Col. ".$sucursal['colonia']. "\n");
							$impresora->text("C.P. ".$sucursal['cp']." ".$sucursal['ciudad']. "\n");
							$impresora->text($sucursal['telefono']. "\n");

							#Fecha de impresión
							$impresora->text(date("d-m-Y H:i") . "\n\n");
							$impresora->setJustification(Printer::JUSTIFY_LEFT);
							$impresora->text("CANT...DESCRIPCION.......IMPORTE\n"); 
							$impresora->text("------------------------------" . "\n"); 
							 
							/*Impresión de los productos*/
							 
                				$listaProducto = json_decode($venta["productos"], true);

                				foreach ($listaProducto as $key => $value) {

                  				$respuesta = ControladorProductos::ctrMostrarProductos("id", $value["id"]);
								// 	/*Alinear a la izquierda para la cantidad y el nombre*/
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
								    $impresora->text("  ".$value["cantidad"]. "    " . $value["descripcion"]. "\n");
								 
								//     /*Y a la derecha para el importe*/
								    $impresora->setJustification(Printer::JUSTIFY_RIGHT);
								    $impresora->text('$'.number_format($value["total"],2)."\n");
        						}
							 
							/* FIN de los productos
							   Impresion del total*/

							$impresora->text("--------\n");
							$impresora->text("TOTAL: $". number_format($venta['total'],2) ."\n");

								$impresora->text("SU PAGO: $". number_format($pagos['cantidad'],2) ."\n");
								if ($pagos['tipopago_id'] == 1) {
									$impresora->text("SU CAMBIO: $".$pagos["datooperacion"]."\n");
									# Alineacion al centro de lo próximo que imprimamos
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
									$impresora->text("Forma de Pago: \n".$tipopago['nombre']);
								} else{

									# Alineacion al centro de lo próximo que imprimamos
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
									$impresora->text("Forma de Pago: \n".$tipopago['nombre']);
									$impresora->text("\nID Transaccion: ".$pagos["datooperacion"]."\n");
								}
							
						 
							#PIE DE PAGINA
							$impresora->setJustification(Printer::JUSTIFY_CENTER);
							$impresora->text("\n\nGracias por su compra\n¡BUEN PROVECHO!\n\n");

							$impresora->text("ID Venta: ".$venta['codigo']."\n");
							$impresora->text("LE ATENDIO: ".$usuario."\n");
							// $impresora->text("LUNES A SABADO DE ".strftime("%H:%M", $horaapertura)."-".strftime("%H:%M", $horacierre));
							$impresora->text("LUNES A DOMINGO DE 7AM A 10PM");
							$impresora->text("\n¡SIGUENOS EN FACEBOOK!\n");

							/*Alimentamos el papel 3 veces*/
							$impresora->feed(3);
							 
							/*Corta el papel, solo si la impresora
								tiene soporte para ello sino no generará
								ningún error.*/
							$impresora->cut();

							if ($venta['total']>="hola") {
#Encabezado							 
							$impresora->text("¡FELICIDADES!"."\n");
							$impresora->text("Gracias a tu compra"."\n"."tienes la oportunidad de participar"."\n"."en nuestra rifa."."\n");
							$impresora->text("NO. BOLETO"."\n");
							$impresora->text("#".$venta['codigo']. "\n\n");
							// $impresora->text("#__________________________\n\n");
							$impresora->text("Para completar tu participacion:"."\n");

							$impresora->text("- Dale like a nuestra pagina."."\n");

							$impresora->text("-Tomate una foto con tu compra "."\n"."en la sucursal o en tu hogar"."\n");

							$impresora->text("-Publicala en nuestro muro.". "\n");

							$impresora->text("-Llena este boleto con "."\n"."tus datos y ponlo en la tombola.". "\n");

							$impresora->text("¡Y listo así, "."\n"." ya estarás participando!". "\n\n");

							$impresora->text("El sorteo será el día "."\n"."14 DE MARZO a las 8:30 PM,". "\n");

							$impresora->text("atraves de nuestra pagina"."\n"." de Facebook.". "\n");

							$impresora->text("\nPara mayor informacion \nAl numero 4442992823\n\n");

							$impresora->text("Visita nuestra nueva sucursal en"."\n"."San Antonio #209 Col. San Felipe\n");

							#Fecha de impresión
							$impresora->text("----------------------". "\n");
							$impresora->text("NO. BOLETO"."\n\n");
							$impresora->text("#".$venta['codigo']. "\n");
							// $impresora->text("#____________________\n\n\n");

							$impresora->setJustification(Printer::JUSTIFY_LEFT);
							$impresora->text("Nombre: ______________________". "\n\n\n");
							$impresora->text("Facebook: ____________________". "\n\n\n");
							$impresora->text("Telefono: ____________________". "\n");


							#Fecha de impresión
							$impresora->text("\n\nImpresion: ".date("d-m-Y H:i"));
								}
								/* La impresora manda un pulso, útil cuando se tiene conectado algun cajón */
								$impresora->pulse();
								 
								/* Para ejecutar la impresión cerramos la conexión con la impresora, siempre debe ir al final.*/
								$impresora->close();
	}


static public function ctrConcurso(){
		// echo $venta;
		$venta = ControladorVentas::ctrMostrarVentas("id", $_GET['idVenta']);

		$nombreImpresora = "Cocecopro"; 
		$conexion = new WindowsPrintConnector($nombreImpresora);
		$impresora = new Printer($conexion);

							# Alineacion al centro de lo próximo que imprimamos
							$impresora->setJustification(Printer::JUSTIFY_CENTER);

							#Se trata de cargar e imprimir el logo.
							try{
								$logo = EscposImage::load("vistas/img/plantilla/logo.png", false);
							    $impresora->bitImage($logo);
							}catch(Exception $e){/*No hacemos nada si hay error*/}
							 
							#Encabezado							 
							$impresora->text("¡FELICIDADES!" . "\n");
							$impresora->text("Gracias a tu compra tienes la oportunidad de participar en nuestra rifa." . "\n");
							$impresora->text("NO. BOLETO"."\n");
							$impresora->text("#".$venta['codigo']. "\n\n");
							$impresora->text("Para completar la participacion:"."\n");

							$impresora->text("- Tomate una foto con Naye o tu compra.". "\n");

							$impresora->text("- Dale like a nuestra pagina y sube tu foto felicitandola y agregando tu no. de boleto.". "\n");

							$impresora->text("- Llena este dato con tus datos y ponlo en nuestra tombola.". "\n");

							$impresora->text("- ¡Y listo así, ya estarás participando!". "\n");

							$impresora->text("El sorteo será el día 14 DE MARZO a las 8:30 PM.". "\n");

							$impresora->text("Atraves de nuestra pagina de Facebook.". "\n");

							$impresora->text("\n\nVisita nuestra nueva sucursal en San Antonio #209 Colonia San Felipe\n\n");
							#Fecha de impresión
							$impresora->text("----------------------". "\n");
							$impresora->text("NO. BOLETO"."\n");
							$impresora->text("#".$venta['codigo']. "\n");
							$impresora->text("Nombre: ______________". "\n");
							$impresora->text("Direccion: ______________". "\n");
							$impresora->text("_______________________". "\n");
							$impresora->text("Telefono: ______________". "\n");
							$impresora->text("----------------------". "\n");
							$impresora->setJustification(Printer::JUSTIFY_LEFT);
							$impresora->text("CANT...DESCRIPCION.......IMPORTE\n"); 
							$impresora->text("------------------------------" . "\n"); 

							#Fecha de impresión
							$impresora->text(date("d-m-Y H:i") . "\n\n");

							/*Alimentamos el papel 3 veces*/
							$impresora->feed(3);
							 
							/*Corta el papel, solo si la impresora
								tiene soporte para ello sino no generará
								ningún error.*/
							$impresora->cut();
							 
							/* La impresora manda un pulso, útil cuando se tiene conectado algun cajón */
							$impresora->pulse();
							 
							/* Para ejecutar la impresión cerramos la conexión con la impresora, siempre debe ir al final.*/
							$impresora->close();
	}


	static public function ctrReImprimir($venta, $idSucursal, $usuario){
		// echo $venta;
		$sucursalActual = ControladorSucursal::ctrMostrarSucursales("id",$idSucursal);
		$venta = ControladorVentas::ctrMostrarVentas("id", $venta);
		$sucursal = ControladorSucursal::ctrMostrarSucursales("id",$venta['sucursal_id']);
		$pagos = ControladorPago::ctrPagosTicket("venta_id", $venta['id']);
		$tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$pagos['tipopago_id']);

		$nombreImpresora = "Cocecopro"; 
		$conexion = new WindowsPrintConnector($nombreImpresora);
		$impresora = new Printer($conexion);

							# Alineacion al centro de lo próximo que imprimamos
							$impresora->setJustification(Printer::JUSTIFY_CENTER);
							$impresora->text(":::::REIMPRESION:::::" . "\n");
							$impresora->text(date("d-m-Y H:i") . "\n");
							$impresora->text("Generada por: ".$usuario."\n");
							$impresora->text("Sucursal: ".$sucursalActual['nombre']."\n");
							# Alineacion al centro de lo próximo que imprimamos
							$impresora->setJustification(Printer::JUSTIFY_CENTER);

							/*
								IMPRESION DEL LOGOTIPO (Opcional)
								No funcionará en todas las impresoras
							 
								Nota: Recomendable que la imagen no sea
								transparente (aunque sea png hay que quitar el canal alfa)
								y que tenga una resolución baja. En mi caso
								la imagen que uso es de 250 x 250
							*/

							#Se trata de cargar e imprimir el logo.
							try{
								$logo = EscposImage::load("vistas/img/plantilla/logo.png", false);
							    $impresora->bitImage($logo);
							}catch(Exception $e){/*No hacemos nada si hay error*/}
							 
							#Encabezado							 
							$impresora->text("COCINA ECONOMICA" . "\n");
							$impresora->text("Con Próposito" . "\n");
							$impresora->text($sucursal['direccion']."\n");
							$impresora->text("Col. ".$sucursal['colonia']. "\n");
							$impresora->text("C.P. ".$sucursal['cp']." ".$sucursal['ciudad']. "\n");
							$impresora->text($sucursal['telefono']. "\n");

							#Fecha de impresión
							$impresora->text("Fecha Venta: ".$venta["fechaalta"]. "\n\n");
							$impresora->setJustification(Printer::JUSTIFY_LEFT);
							$impresora->text("CANT...DESCRIPCION.......IMPORTE\n"); 
							$impresora->text("------------------------------" . "\n"); 
							 
							/*Impresión de los productos*/
							 
                				$listaProducto = json_decode($venta["productos"], true);

                				foreach ($listaProducto as $key => $value) {

                  				$respuesta = ControladorProductos::ctrMostrarProductos("id", $value["id"]);
								// 	/*Alinear a la izquierda para la cantidad y el nombre*/
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
								    $impresora->text("  ".$value["cantidad"]. "    " . $value["descripcion"]. "\n");
								 
								//     /*Y a la derecha para el importe*/
								    $impresora->setJustification(Printer::JUSTIFY_RIGHT);
								    $impresora->text('$'.number_format($value["total"],2)."\n");
        						}
							 
							/* FIN de los productos
							   Impresion del total*/

							$impresora->text("--------\n");
							$impresora->text("TOTAL: $". number_format($venta['total'],2) ."\n");

								$impresora->text("PAGO: $". number_format($pagos['cantidad'],2) ."\n");
								if ($pagos['tipopago_id'] == 1) {
									$impresora->text("Su cambio fue: $".$pagos["datooperacion"]."\n");
									# Alineacion al centro de lo próximo que imprimamos
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
									$impresora->text("Forma de Pago: \n".$tipopago['nombre']);
								} else{

									# Alineacion al centro de lo próximo que imprimamos
									$impresora->setJustification(Printer::JUSTIFY_LEFT);
									$impresora->text("Forma de Pago: \n".$tipopago['nombre']);
									$impresora->text("\nID Transaccion: ".$pagos["datooperacion"]."\n");
								}
							
						 
							#PIE DE PAGINA
							$impresora->setJustification(Printer::JUSTIFY_CENTER);
							$impresora->text("ID Venta: ".$venta['codigo']."\n");
							// $impresora->text("LE ATENDIO: ".$cajero->name." ".$cajero->lastname."\n");
							// $impresora->text("LUNES A SABADO DE ".strftime("%H:%M", $horaapertura)."-".strftime("%H:%M", $horacierre));
							$impresora->text("LUNES A DOMINGO DE 7AM A 10PM");
							$impresora->text("\n¡SIGUENOS EN FACEBOOK!\n");

							/*Alimentamos el papel 3 veces*/
							$impresora->feed(3);
							 
							/*Corta el papel, solo si la impresora
								tiene soporte para ello sino no generará
								ningún error.*/
							$impresora->cut();
							 
							/* La impresora manda un pulso, útil cuando se tiene conectado algun cajón */
							$impresora->pulse();
							 
							/* Para ejecutar la impresión cerramos la conexión con la impresora, siempre debe ir al final.*/
							$impresora->close();
	}
}
?>