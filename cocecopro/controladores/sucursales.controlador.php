<?php


class ControladorSucursal{

	/*=============================================
	MOSTRAR SUCURSALES
	=============================================*/

	static public function ctrMostrarSucursales($item, $valor){

		$tabla = "sucursal";

		$respuesta = ModeloSucursal::mdlMostrarSucursales($tabla, $item, $valor);

		return $respuesta;

	}


	static public function ctrSucursalesMostrar($item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		$tabla = "sucursal";

		$respuesta = ModeloSucursal::mdlSucursalesMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5);

		return $respuesta;
		
	}



	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function ctrRangoFechasSucursal($fechaInicial, $fechaFinal){

		$tabla = "sucursal";

		$respuesta = ModeloSucursal::mdlRangoFechasSucursal($tabla, $fechaInicial, $fechaFinal);

		return $respuesta;
		
	}


	/*=============================================
	CREAR SUCURSAL
	=============================================*/
	static public function ctrCrearSucursal(){

		if(isset($_POST["nuevoSucursal"])){

			/*=============================================
			ACTUALIZAMOS SUCURSAL
			=============================================*/	

			$item = "id";
			$valor = $_SESSION["id"];
			$item1b = "ultimo_movimiento";
			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			$valor1b = $fecha.' '.$hora;

			$tabla = "sucursal";

			$datos = array(
							   "nombre"=>$_POST["nuevoSucursal"],
					           "email"=>$_POST["nuevoEmail"],
					           "telefono"=>$_POST["nuevoTelefono"],
					           "direccion"=>$_POST["direccion"],
					           "estado"=>$_POST["estado"],
					           "municipio"=>$_POST["municipio"],
					           "colonia"=>$_POST["colonia"],
					           "cp"=>$_POST["cp"],
					           "usuario_id"=>$valor,
						   	   "fechaalta"=>$valor1b
						   	);

			   	$respuesta = ModeloSucursal::mdlIngresarSucursal($tabla, $datos);
			   	// echo $respuesta; exit();
			   	if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El cliente ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "sucursales";

									}
								})

					</script>';

				}else {

					echo'<script>

					swal({
						  type: "wrong",
						  title: "'.$respuesta.'La Sucursal no ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "sucursales";

									}
								})

					</script>';

				}

		}

	}
static public function ctrCrearSucursal2(){

		if(isset($_POST["nuevoSucursal"])){

			/*=============================================
			ACTUALIZAMOS SUCURSAL
			=============================================*/	

			$item = "id";
			$valor = $_SESSION["id"];
			$item1b = "ultimo_movimiento";
			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			$valor1b = $fecha.' '.$hora;

			$fechaUsuario = ControladorUsuarios::ctrActualizarUsuario($item1b, $valor1b, $item, $valor);

			/*=============================================
			GUARDAR CORTE
			=============================================*/	

			$tabla = "sucursal";

			$datos = array("nombre"=>$_POST["nuevoSucursal"],
					           "email"=>$_POST["nuevoEmail"],
					           "telefono"=>$_POST["nuevoTelefono"],
					           "direccion"=>$_POST["direccion"],
					           "usuario_id"=>$valor,
						   	   "fechaalta"=>$valor1b);

			$ingresa = ModeloSucursal::mdlIngresarSucursal($tabla, $datos);
				$item2 = "usuario_id";
				$max = ModeloSucursal::mdlMaxIDSucursal($tabla, "usuario_id", $valor);
				// echo "NUM: ".$max['idmax']; exit();

				$nuevasucursal = ModeloSucursal::mdlMostrarSucursales($tabla, $item, $max['idmax'], null, null, null, null, null, null, null, null);

			if($respuesta == "ok"){
				
				echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "success",
					  title: "La sucursal numero: '.$nuevasucursal['codigo'].' alias: "'.$nuevasucursal['nombre'].'" ha sido creada correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "sucursales";

								}
							})

				</script>';

			}else{
								echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "error",
					  title: "La sucursal no ha podido generarse",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "#";

								}
							})

				</script>';
			}

		}

	}

	/*=============================================*/
	
static public function ctrMaxIDSucursal($item, $valor){

		$tabla = "sucursal";

		$respuesta = ModeloSucursal::mdlMaxIDSucursal($tabla, $item, $valor);

		return $respuesta;

}

	/*=============================================
	MOSTRAR SUCURSALES
	=============================================*/

	static public function ctrMaxIDVenta($item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		$tabla = "sucursal";

		$respuesta = ModeloSucursal::mdlMaxIDVenta($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5);

		return $respuesta;
		
	}


}