UPDATE `sis_inventario`.`ventas` SET `impuesto` = 0

SELECT precio_venta, price_out
FROM productos
LEFT JOIN productaago19 ON productaago19.id = productos.id

SELECT SUM(total) FROM `sell`
WHERE total IS NOT NULL

SELECT ventas.id,productaago19.id,productaago19.name,operation.q,productaago19.price_out,productaago19.price_out*operation.q FROM `ventas` LEFT JOIN operation ON operation.sell_id = ventas.id LEFT JOIN productaago19 ON operation.product_id = productaago19.id WHERE ventas.id <=625 ORDER BY ventas.id ASC 

SET @numero=0;

SELECT id, @numero:=@numero+1 AS `#`, CONCAT(IF(@numero>9, '0', '00'),@numero), CONCAT('UPDATE productos SET codigo=',id_categoria,IF(@numero>9, '0', '00'),@numero, ' WHERE productos.id=', productos.id, ';')
FROM `productos` WHERE id_categoria = 7


SELECT CONCAT('UPDATE ventas SET fechaalta = "',fecha,'" WHERE id=',id,';') FROM `ventas`

SELECT
	sum(total), sum(estatusventa) 
FROM
	`ventas` 
WHERE
	fechaalta BETWEEN '2020-02-01 00:00:00' 
	AND '2020-02-01 23:59:59'
	
	
select sell.id, SUM(operation.q),SUM(productos.precio_venta),SUM(productaago19.price_out), SUM(operation.q*productaago19.price_out),sell.total, sell.created_at, IF
	(
	 SUM(operation.q*productaago19.price_out) = sell.total,
	'IGUALES',
	'NO IGUALES')
from sell
LEFT JOIN operation ON operation.sell_id = sell.id
LEFT JOIN productaago19 ON productaago19.id = operation.product_id
LEFT JOIN productos ON productos.id = operation.product_id
GROUP BY sell.id
ORDER BY sell.id

SET @numero = 1;
SELECT
	cortecaja.id,
	CONCAT('UPDATE cortecaja	SET codigo=1',
IF
	(
	@numero <= 9,
	'0000000',
IF
	( @numero <= 99, '000000', IF ( @numero <= 999, '00000', '0000' ) ) 
	),
	@numero,
	' WHERE cortecaja.id=', cortecaja.id,';'
	) AS codigo,
	@numero := @numero + 1 AS `contador`
FROM
	`cortecaja`
ORDER BY
	id ASC
	
	

SET @numero = 1;
SELECT
	ventas.id,
	CONCAT('UPDATE ventas SET codigo=1',
IF
	(
	@numero <= 9,
	'0000000',
IF
	( @numero <= 99, '000000', IF ( @numero <= 999, '00000', '0000' ) ) 
	),
	@numero,
	', neto=',ventas.total,',metodo_pago="',paytype.name,'" WHERE ventas.id=', ventas.id,';'
	) AS codigo,
	@numero := @numero + 1 AS `contador`
FROM
	`ventas`
	LEFT JOIN paytype ON paytype.id = ventas.paytype_id 
ORDER BY
	id ASC
	
	
	
SET @numero = 1;
SELECT
	sell.id,
	CONCAT(
	'1',
IF
	(
	@numero <= 9,
	'0000000',
IF
	( @numero <= 99, '000000', IF ( @numero <= 999, '00000', '0000' ) ) 
	),
	@numero 
	) AS codigo,
	user_id AS id_cliente,
	cajero_id AS id_vendedor,
	productos.precio_venta*operation.q as totalxproducto,
	paytype.name AS metodo_pago,
	sell.total AS total,
	sell.created_at as fecha,
	@numero := @numero + 1 AS `contador`
FROM
	`sell`
	LEFT JOIN paytype ON paytype.id = sell.paytype_id 
	LEFT JOIN operation ON operation.sell_id = sell.id 
	LEFT JOIN productos ON operation.product_id = productos.id 
ORDER BY
	id ASC
	
	
	SELECT CONCAT("UPDATE ventas SET productos='",auxiliar.productos,"]' WHERE ventas.id=", auxiliar.id, ";")
FROM ventas
LEFT JOIN auxiliar ON auxiliar.id = ventas.id


SELECT sell.id, sell.q as qsell, sell.total, operation.q as qo, operation.sell_id, operation.product_id, sell.is_applied, sell.user_id, sell.cajero_id
from `sell`
left join operation on operation.sell_id = sell.id
where sell.total is null

SELECT
	productos.id idpro,
	ventas.id as ventaid,
	SUM( operation.q ),
	productos.precio_venta,
	productos.descripcion,
	productaago19.price_out,
	operation.q * productaago19.price_out,
	ventas.total,
	ventas.fecha 
FROM
	ventas
	LEFT JOIN operation ON operation.sell_id = ventas.id
	LEFT JOIN productaago19 ON productaago19.id = operation.product_id
	LEFT JOIN productos ON productos.id = operation.product_id 
WHERE
	ventas.corte_id IS NOT NULL 
	AND ventas.is_applied = 1 
	AND productos.id IS NOT NULL
GROUP BY
	productos.id 
ORDER BY
	productos.id
	
	SELECT CONCAT("UPDATE ventas SET productos='",auxiliar.productos,"]' WHERE ventas.id=", auxiliar.id, ";")
FROM ventas
LEFT JOIN auxiliar ON auxiliar.id = ventas.id


SELECT
CONCAT("UPDATE productos SET ventas=",SUM( operation.q )," WHERE productos.id=", productos.id, ";")	
FROM
	ventas
	LEFT JOIN operation ON operation.sell_id = ventas.id
	LEFT JOIN productos ON productos.id = operation.product_id 
WHERE
	ventas.corte_id IS NOT NULL 
	AND ventas.is_applied = 1 
	AND productos.id IS NOT NULL
GROUP BY
	productos.id 
ORDER BY
	productos.id
	
	
	SELECT sell.id, sell.q as qsell, sell.total, operation.q as qo, operation.sell_id, operation.product_id, sell.is_applied, sell.user_id, sell.cajero_id, sell.is_applied
from `sell`
left join operation on operation.sell_id = sell.id
where sell.total is null


SET @numero = 1;
SELECT
	ventas.id,
	CONCAT('UPDATE ventas SET 
	 neto=',ventas.total,', metodo_pago="',paytype.name,'" WHERE ventas.id=', ventas.id,';'
	) AS codigo,
	@numero := @numero + 1 AS `contador`
FROM
	`ventas`
	LEFT JOIN paytype ON paytype.id = ventas.paytype_id 
ORDER BY
	id ASC
	
SELECT CONCAT("UPDATE ventas SET productos='",auxiliar.productos,"]' WHERE ventas.id=", auxiliar.id, ";")
FROM ventas
LEFT JOIN auxiliar ON auxiliar.id = ventas.id
WHERE auxiliar.id <=6067


SET @numero=0;

SELECT @numero:=@numero+1 AS `#`,ventas.id, pago.tipopago_id as tipopago_pago, ventas.tipopago_id as tipopago_ventas, ventas.estatusventa, pago.fechaalta, ventas.fecha
FROM ventas
LEFT JOIN pago ON pago.venta_id = ventas.id
where pago.tipopago_id is null 
order by ventas.id


SELECT CONCAT('UPDATE ventas SET id_vendedor=',usuario_id,' WHERE id=',id,';') FROM `ventas`

SELECT id,id_vendedor, usuario_id FROM `ventas`
WHERE id_vendedor<>usuario_id

SELECT CONCAT('UPDATE ventas SET fechaalta = "',fecha,'" WHERE id=',id,';') FROM `ventas`

SET @numero=0;

SELECT @numero:=@numero+1 AS `#`,ventas.id, pago.tipopago_id as tipopago_pago, ventas.tipopago_id as tipopago_ventas, ventas.estatusventa, pago.fechaalta, ventas.fecha
FROM ventas
LEFT JOIN pago ON pago.venta_id = ventas.id
where pago.tipopago_id is null 
order by ventas.id

SELECT ventas.id, ventas.id_vendedor, pago.cajero_id, pago.usuario_id FROM ventas
LEFT JOIN pago ON pago.venta_id = ventas.id
order by ventas.id

SELECT sum(estatusventa) AS `#` FROM `ventas`

SELECT CONCAT("UPDATE ventas SET productos='",auxiliar.productos,"]' WHERE ventas.id=", auxiliar.id, ";")
FROM ventas
LEFT JOIN auxiliar ON auxiliar.id = ventas.id

SELECT id,id_vendedor, usuario_id FROM `ventas`
WHERE id_vendedor<>usuario_id

UPDATE `cocecopro`.`ventas` SET `act` = 1 